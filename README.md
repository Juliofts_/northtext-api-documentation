# New NorthText API

## Authentication

API key

  - API key is found in top-right side of the screen **"Authentication"**
  - API key must be passed with each request as a POST variable (form field) or query string parameter (URL variable)
  - Variable name must always be **token**

## Examples
Sample API key: **abc123**

#### list groups

GET:

```
https://northtextbeta.azurewebsites.net/group/list?token=abc123&groupid=4
```

#### send a single message

POST: 

```
https://northtextbeta.azurewebsites.net/message/send
```

field | value
---------- | ----------
token | abc123
body | test message
to | +16193210987


# API endpoints
##### The base URL for all endpoints is
```
https://northtextbeta.azurewebsites.net/
```


#### /api/v1/user/validateUser

``
GET https://northtextbeta.azurewebsites.net/api/v1/user/validateUser
``

Validate if your account is valid and has access to our system.


field | description | type | format
---------- | ---------- | ---------- | ----------
user_no | optional, **required** if username is empty. | string | 240 characters
username | optional, **required** if user_no is empty. | string | 240 characters
password | optional, **required** if you want to validate the password. | string | 240 characters
mobile_number | **required** | string | +1234567890



JSON response example

```
Status: 200 Ok

{
    "success": true,
    "response": {
        "validateUserResult": true
    }
}
```


#### /api/updateUserStatus

``
POST https://northtextbeta.azurewebsites.net/api/updateUserStatus
``

This method allows you To Set a phone number To Inactive.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
mobile_number | **required** | string | +1234567890
status   | **required** | int     | 1


#### /api/updateQueueStatus

``
POST https://northtextbeta.azurewebsites.net/api/updateQueueStatus
``

This method allows you to update the status of a queue. Utilize the getMessages() method to download the queue.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
queue_no | **required** | string | +1234567890
status   | **required** | int     | 1

#### /api/updateInboxItem

``
POST https://northtextbeta.azurewebsites.net/api/updateInboxItem
``

This method allows you to update your inbox. (you can pass the following for status: new, read, unread, deleted)


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
message_log_no | **required** | double | 2.0
status   | **required** | int     | 1

#### /api/unsubscribeNumber

``
POST https://northtextbeta.azurewebsites.net/api/unsubscribeNumber
``

This method allows you to unsubscribe a user from your account.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
mobile_number | **required** | string | +1234567890

#### /api/testLogic

``
POST https://northtextbeta.azurewebsites.net/api/testLogic
``

This method allows you to test keyword logic for text replies only.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
msg | **required** | string | 240 characters

#### /api/sendMessageToGroupOption

``
POST https://northtextbeta.azurewebsites.net/api/sendMessageToGroupOption
``

This method allows you to send messages to specified group(s). You can pass multiple values separated by a comma in the groups attribute. You can choose whether or not to include their first name in the message.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
text_message | **required** | string | 240 characters
groups | **required** | string | 240 characters
includeName | **required** | boolean | 1

#### /api/sendMessageToGroupOption

``
POST https://northtextbeta.azurewebsites.net/api/sendMessageToGroupOption
``

This method allows you to send messages to specified group(s). You can pass multiple values separated by a comma in the groups attribute.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
text_message | **required** | string | 240 characters
groups | **required** | string | 240 characters

#### /api/sendMessageToAllOption

``
POST https://northtextbeta.azurewebsites.net/api/sendMessageToAllOption
``

This method allows you To send messages To specified all subscribers. You can choose whether Or Not To include their first name In the message.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
text_message | **required** | string | 240 characters
includeName | **required** | boolean | 1

#### /api/sendMessageToAll

``
POST https://northtextbeta.azurewebsites.net/api/sendMessageToAll
``

This method allows you To send messages To specified all subscribers.


field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
text_message | **required** | string | 240 characters

#### /api/parseDateTime

``
POST https://northtextbeta.azurewebsites.net/api/parseDateTime
``

This method will parse the date and time from a string, including day of week, month, and human logic such as 'tomorrow', '15 minutes', etc.

field |  | type | format
---------- | ---------- | ---------- | ----------
msg | **required** | string | 240 characters

#### /api/isKeywordAvailable

``
POST https://northtextbeta.azurewebsites.net/api/isKeywordAvailable
``

This method determines if the keyword is available for your account. Set process_type to either Private or Global

field |  | type | format
---------- | ---------- | ---------- | ----------
logic_keyword | **required** | string | 240 characters
user_no       | **required** | double | 2.0
process_type  | **required** | string | 240 characters

#### /api/getUserGroups

``
POST https://northtextbeta.azurewebsites.net/api/getUserGroups
``

This method will return the groups assigned to your account.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
user_no       | **required** | double | 2.0

#### /api/getMessagesByStatus

``
POST https://northtextbeta.azurewebsites.net/api/getMessagesByStatus
``

This method will display the next 50 messages we are sending out for your account by Status.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
user_no       | **required** | double | 2.0
status   | **required** | int     | 1

#### /api/getMessages

``
POST https://northtextbeta.azurewebsites.net/api/getMessages
``

This method will display the next 50 messages we are sending out for your account.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
user_no       | **required** | double | 2.0

#### /api/createMessageForMultipleNumbers

``
POST https://northtextbeta.azurewebsites.net/api/createMessageForMultipleNumbers
``

This method allows you to send messages through our API.

field |  | type | format
---------- | ---------- | ---------- | ----------
phone_number | **required** | string | +1234567890
number_name  | **required** | string | 240 characters

#### /api/createMessage

``
POST https://northtextbeta.azurewebsites.net/api/createMessage
``

This method allows you to send messages through our API.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
phone_number | **required** | string | +1234567890
number_name  | **required** | string | 240 characters
text_message | **required** | string | 240 characters
send_on      | **required** | timestamp | 2020-01-20 16:30:00 (4:30 PM on 01/20/2020)

#### /api/addNumberToGroup

``
POST https://northtextbeta.azurewebsites.net/api/addNumberToGroup
``

This method allows you add a number to a specified group name.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
phone_number | **required** | string | +1234567890
group_name  | **required** | string | 240 characters

#### /api/addNumberEmail

``
POST https://northtextbeta.azurewebsites.net/api/addNumberEmail
``

This method allows you to add phone numbers to your account.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
phone_number | **required** | string | +1234567890
number_name  | **required** | string | 240 characters
email | **required** | string | 240 characters
welcome_message | **required** | boolean | 1

#### /api/addNumber

``
POST https://northtextbeta.azurewebsites.net/api/addNumber
``

This method allows you to add phone numbers to your account.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
phone_number | **required** | string | +1234567890
number_name  | **required** | string | 240 characters
welcome_message | **required** | boolean | 1

#### /api/addIncomingMessage

``
POST https://northtextbeta.azurewebsites.net/api/addIncomingMessage
``

This method allows you to add messages to your inbox and process logic against them.

field |  | type | format
---------- | ---------- | ---------- | ----------
user_no       | **required** | double | 2.0
message_text | **required** | string | 240 characters
phone_number | **required** | string | +1234567890
message_itemid  | **required** | string | 240 characters
message_received | **required** | timestamp | 2020-01-20 16:30:00 (4:30 PM on 01/20/2020)
message_sent | **required** | timestamp | 2020-01-20 16:30:00 (4:30 PM on 01/20/2020)
message_to | **required** | string | +1234567890


#### /api/ValidatePhoneNumber

``
POST https://northtextbeta.azurewebsites.net/api/addIncomingMessage
``

This method will validate that you have access to send to the phone number provided based on country and area code.

field |  | type | format
---------- | ---------- | ---------- | ----------
token | **required** | string | abc123
phone_number | **required** | string | +1234567890

